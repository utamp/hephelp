import numpy as np
import matplotlib
from matplotlib.pyplot import *
from mpl_toolkits.axes_grid1 import make_axes_locatable
import copy
from numpy import random

def ratio_err(m1, m2):
    """ Returns a tuple with the ratio and the uncertainty on the ratio of two quantities. 
    The uncertainties are assumed to be symmetric.

    Parameters
    ---------
    
    m1: tuple
    first measurement, as a tuple (measurement, error)

    m2: tuple
    second measurement, as a tuple (measurement, error)


    Returns
    -------
    a tuple (m1/m2, sigma(m1/m2))
    """

    # derivatives
    drdm1 = 1/m2[0]
    drdm2 = -m1[0]/(m2[0]**2)

    # usual error propagation
    sigma = np.sqrt((m1[1]*drdm1)**2 + (m2[1]*drdm2)**2)

    return (m1[0]/m2[0],  sigma)
    


def binomial_eff(k, N):
    """ Returns the efficiency and its 68% confidence interval limits

    Parameters
    ---------
    
    k: float
    number of events passing the selection

    N
    number of trials

    Returns
    -------
    efficiency, upper edge, lover edge
    """

    if k < N:
        eff = k/N # this is the efficiency                                                                                                                                                                                                                                
        dist = np.abs(0.5 -np.abs(eff - 0.5))
        lol = random.binomial(N, eff, size=int(min(1000000, 100000/dist)) )

        Ma = np.percentile(lol, 100-(32/2))/N
        Mi = np.percentile(lol, 0+(32/2))/N
        eff = np.percentile(lol, 50)/N

        return eff, Ma, Mi
    else:
        return np.nan, np.nan, np.nan


class dataset:
    """ A class to hold all the important information of a specific dataset
    """
    def __init__(self, name, label, dataframe, root_file, scale, color, style, cut = None):
        self.name = name # name of the dataset (string)
        if cut:
            self.dataframe = dataframe.query(cut) # the pandas dataframe with the data
        else:
            self.dataframe = dataframe
        self.scale = scale # the scale factor to apply to this dataset
        self.color = color # the color for plotting
        self.style = style # the plot style
        self.label = label # the plot label associated to this DF
        self.root_file = root_file # the original root file (if any)



def plotterWithResidualFromDataset(dataset_list, 
                                   variable,
                                   xrange, 
                                   bins, 
                                   signalProcesses=[], 
                                   dataProcess=None,
                                   excludedProcessesFromMCHistos = [], 
                                   cut_data = None,
                                   cut_mc = None,
                                   cut_signal = None,
                                   weight_variable = '__weight__',
                                   yrange = None,
                                   xlabel_ = None, 
                                   sigScale = 1, 
                                   globalMCScale=1, 
                                   figsize = (8,8),
                                   legend_columns= 3,
                                   yrange_comp=None,
                                   comparison = 'ratio',
                                   figname=None):
    """ A function to plot a quantity from a list of dataset objects, including the residual between data and MC

    Parameters
    ---------
    
    dataset_list: list
    the list of datasets to plot
                                   
    variable: string
    the name of the variable to plot

    
    signalProcesses: list
    list of the names of the datasets that must be considered signal MC

    dataProcess: string
    name of the dataset that must be considered data. Only one is allowed, otherwise the residual definition would be unclear.
 
    excludedProcessesFromMCHistos: list
    list of the names of the datasets that must be excluded from the MC plot

    cut_data: string
    the selection to be applied to the dataframes containing data. Internally this is passed to a dataframe.query()

    cut_mc : string
    the selection to be applied to the dataframes containing MC. Internally this is passed to a dataframe.query()

    cut_signal : string
    the selection to be applied to the dataframes containing the signal MC. Internally this is passed to a dataframe.query()

    sigScale: float
    scale factor applied to the signal MC

    globalMCScale: float
    a scale factor to be applied to all generic MC processes on top of the one stored in the dataset itself.
   
    xrange : tuple
    plotting range of the x axis, in the form (xmin, xmax)

    yrange : tuple
    plotting range of the y axis, in the form (ymin, ymax)

    bins: int
    number of bins for the plot
                                  
    xlabel: string
    label of the x axis
                   
    figsize: tuple
    the figure size as (x_side,y_side)

    figname: string
    figure name


    Returns
    -------
    a figure
    """

    fig, axes = subplots(nrows=1, ncols=1, figsize=figsize, sharey=False, sharex=False)
    ax = axes
    divider = make_axes_locatable(ax)
    
    
    mcDatasetList = [dataset for dataset in dataset_list if dataset.name not in excludedProcessesFromMCHistos and dataset.name not in signalProcesses ]
    signalDatasets = [dataset for dataset in dataset_list if dataset.name in signalProcesses]

 

    # MC
    mclist = 0
    bn = 0
    dummy = 0
    w2 = 0

    if cut_mc:
        mclist, bn, dummy = ax.hist([d.dataframe.query(cut_mc)[variable] for d in mcDatasetList], 
                            weights = [globalMCScale*d.dataframe.query(cut_mc)[weight_variable]*d.scale for d in mcDatasetList],
                            bins = bins, 
                            range = xrange, 
                            color = [d.color for d in mcDatasetList],
                            log = False,
                            stacked = True,
                            label = [d.label for d in mcDatasetList]
                           )   
        for d in mcDatasetList:
            rc, dummy = np.histogram(d.dataframe.query(cut_mc)[variable], weights = (globalMCScale*d.scale*d.dataframe.query(cut_mc)[weight_variable])**2, bins = bins, range = xrange)
            w2 = w2 + np.array(rc)
    else:
        mclist, bn, dummy = ax.hist([d.dataframe[variable] for d in mcDatasetList], 
                            weights = [globalMCScale*d.dataframe[weight_variable]*d.scale for d in mcDatasetList],
                            bins = bins, 
                            range = xrange, 
                            color = [d.color for d in mcDatasetList],
                            log = False,
                            stacked = True,
                            label = [d.label for d in mcDatasetList]
                           )   
        for d in mcDatasetList:
            rc, dummy = np.histogram(d.dataframe[variable], weights = (globalMCScale*d.scale*d.dataframe[weight_variable])**2, bins = bins, range = xrange)
            w2 = w2 + np.array(rc)

    if cut_signal:
        for signal in signalDatasets:
            ax.hist(signal.dataframe.query(cut_signal)[variable], 
                    weights = signal.scale*sigScale*signal.dataframe.query(cut_signal)[weight_variable], 
                    bins = bins,
                    range= xrange,
                    color=signal.color,
                    log=False,
                    stacked=False,
                    histtype='step',
                    label = signal.label,
                    linewidth=2
                )   
    else:
        for signal in signalDatasets:
            ax.hist(signal.dataframe[variable], 
                    weights =  signal.scale*sigScale*signal.dataframe[weight_variable], 
                    bins = bins,
                    range= xrange,
                    color=signal.color,
                    log=False,
                    stacked=False,
                    histtype='step',
                    label = signal.label,
                    linewidth=2
                )   
    # bin width                      
    bw = bn[1]-bn[0]
    err = 0
    mc = 0
    prevcounts = 0
    mc = mclist[-1]

    '''
    for scale, counts, w2 in zip([d.scale for d in mcDatasetList], mclist, rawcounts):
        w_counts =  (np.array(counts) - prevcounts)/(scale*globalMCScale) # remove the correlated parts
        w = w_counts/counts # these are the bin-by-bin weights
        
        mc = mc + (np.array(counts) - prevcounts) # total MC value in each bin
        prevcounts = np.array(counts)

    err = np.sqrt(err)
    '''
    
    err_mc = np.sqrt(w2)

    ax.bar(bn[:-1]+bw/2, 2*err_mc, width=bw, bottom=mc - err_mc,  facecolor=None, alpha = 0, hatch='////')


    if yrange:
        ax.set_ylim(yrange[0], yrange[1])

    if dataProcess:
        ax2 = divider.append_axes("bottom", size='30%', pad=.4)
        ax.figure.add_axes(ax2)

        dataDataset = [dataset for dataset in dataset_list if dataset.name == dataProcess][0]
            
        data = 0

        if cut_data:
            data, bn, dummy = ax.hist(dataDataset.dataframe.query(cut_data)[variable], 
                              bins = bins,
                              range= xrange,
                              color=dataDataset.color,
                              log=False,
                              stacked=False,
                              histtype='step',
                              label = dataDataset.label
                             )   

        else:
            data, bn, dummy = ax.hist(dataDataset.dataframe[variable], 
                              bins = bins,
                              range= xrange,
                              color=dataDataset.color,
                              log=False,
                              stacked=False,
                              histtype='step',
                              label = dataDataset.label
                             )   

        err_data = np.sqrt(data)
            
        # data-mc/mc
        if comparison == "ratio":
            yerr_data =  err_data/mc
            yerr_mc =  err_mc*(data/mc**2)
            yerr_mc = np.nan_to_num(yerr_mc, neginf=0, posinf=0)

            yerr_ratio = np.sqrt(yerr_data**2 + yerr_mc**2)
            
            ax2.errorbar(bn[:-1]+(bn[1]-bn[0])/2., (data-mc)/mc, yerr=yerr_ratio, color="k", linewidth=0, elinewidth=1, fmt='o')
        
            ax2.bar(bn[:-1]+bw/2, 2*yerr_mc, width=bw, bottom=-yerr_mc, color='k', alpha = 0.2)

            ax2.plot([bn[0], bn[-1]], [0,0], color="k", linewidth=2, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [0.5,0.5], color="k", linewidth=1, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [-0.5,-0.5], color="k", linewidth=1, linestyle='--')


            ax2.set_xlim(xrange)
            ax2.set_ylim(-1,1)
            if yrange_comp:
                ax2.set_ylim(yrange_comp[0],yrange_comp[1])
            ax2.set_ylabel(r'$\frac{Data-MC}{MC}$')


        # data-mc
        if comparison == "residual":
            yerr_ratio = np.sqrt(err_data**2 + err_mc**2 )
            yerr_mc =  err_mc
            yerr_data =  err_data

            ax2.errorbar(bn[:-1]+(bn[1]-bn[0])/2., (data-mc), yerr=yerr_ratio, color="k", linewidth=0, elinewidth=1, fmt='o')
            yerr_mc = np.nan_to_num(yerr_mc, neginf=0, posinf=0)
        
            ax2.bar(bn[:-1]+bw/2, 2*yerr_mc, width=bw, bottom=-yerr_mc, color='k', alpha = 0.2)

            ax2.plot([bn[0], bn[-1]], [0,0], color="k", linewidth=2, linestyle='--')
            
            ax2.set_xlim(xrange)
            if yrange_comp:
                ax2.set_ylim(yrange_comp[0],yrange_comp[1])
            ax2.set_ylabel(r'Residual')


        # pull
        if comparison == "pull":
            yerr_ratio = np.sqrt(err_data**2 + err_mc**2 )
            yerr_mc =  err_mc
            yerr_data =  err_data

            ax2.errorbar(bn[:-1]+(bn[1]-bn[0])/2., (data-mc)/yerr_ratio, yerr=0*yerr_ratio, color="k", linewidth=0, elinewidth=1, fmt='o')

            ax2.plot([bn[0], bn[-1]], [0,0], color="k", linewidth=2, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [1,1], color="k", linewidth=1, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [3,3], color="k", linewidth=1, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [-1,-1], color="k", linewidth=1, linestyle='--')
            ax2.plot([bn[0], bn[-1]], [-3,-3], color="k", linewidth=1, linestyle='--')


            ax2.set_xlim(xrange)
            ax2.set_ylim(-5,5)
            if yrange_comp:
                ax2.set_ylim(yrange_comp[0],yrange_comp[1])
            ax2.set_ylabel(r'Pull')


    # legend and cosmetics
    ax.set_xlim(xrange)
    ax.legend(ncol=legend_columns)
    ax.set_ylabel(r'Counts')

    if dataProcess:
        ax.set_xticks([])

    xlabel(xlabel_)
    
    if figname:
        savefig(f'{figname}.pdf')

    return ax


def plotEfficiency(dataset,
                   variable,
                   xrange,
                   bins,
                   cut,
                   yrange = None,
                   xlabel_ = None,
                   figsize = (8,8),
                   figname=None):
    """ A function to plot the efficency of a selection as function of a variable

    Parameters
    ---------
    
    dataset: dataset object
    datasets to plot
                                   
    variable: string
    the name of the variable to plot

    cut: string
    the selection of which you want the efficiency. Internally this is passed to a dataframe.query()
   
    xrange : tuple
    plotting range of the x axis, in the form (xmin, xmax)

    yrange : tuple
    plotting range of the y axis, in the form (ymin, ymax)

    bins: int
    number of bins for the plot
                                  
    xlabel_: string
    label of the x axis
                   
    figsize: tuple
    the figure size as (x_side,y_side)

    figname: string
    figure name

    Returns
    -------
    a figure
    """

    nocut, bn, dummy = hist(dataset.dataframe[variable],
                            bins = bins,
                            range = xrange,
                            log = False,
                           )

    cut, bn, dummy = hist(dataset.dataframe.query(cut)[variable],
                            bins = bins,
                            range = xrange,
                            log = False,
                           )

    binomial_eff_vect = np.vectorize(binomial_eff)
    eff, e_min, e_max = binomial_eff_vect(cut, nocut)

    fig, ax = subplots(nrows=1, ncols=1, figsize=figsize, sharey=False, sharex=False)

    ax.errorbar(bn[1:]-(bn[1]-bn[0])/2., eff, yerr = [eff-e_min, e_max-eff ], linewidth=0, elinewidth=1, color='k', fmt ='o')

    # legend and cosmetics

    ax.set_xlim(xrange)
    ax.set_ylabel(r'$\epsilon$')

    xlabel(xlabel_)

    if figname:
        savefig(f'{figname}.pdf')

    return ax



def calculateFoM(dataset_list,
                 signalProcesses,
                 backgroundProcesses,
                 variable_1,
                 variableEdges_1,
                 weight_variable,
                 variable_2=None,
                 variableEdges_2=None,
                 cut_background = None,
                 cut_signal = None,
                 directions = [1, 1] #1 for "variable > cut", -1 for "variable < cut"          
             ):

    """
    dataset_list: list
    the list of datasets to plot
    
    signalProcesses: list
    list of the names of the datasets that must be considered signal

    backgroundProcesses: list
    list of the names of the datasets that must be considered background

    dataProcess: string
    name of the dataset that must be considered data. Only one is allowed, otherwise the residual definition would be unclear.
 
    excludedProcessesFromMCHistos: list
    list of the names of the datasets that must be excluded from the MC plot

    cut_data: string
    the selection to be applied to the dataframes containing data. Internally this is passed to a dataframe.query()

    cut_mc : string
    the selection to be applied to the dataframes containing MC. Internally this is passed to a dataframe.query()

    cut_signal : string
    the selection to be applied to the dataframes containing the signal MC. Internally this is passed to a dataframe.query()

    sigScale: float
    scale factor applied to the signal MC
    """

    cuts_1=[]
    cuts_2=[]
    foms=[]
    res = [None, None]


    # prepare two separate lists of dataframes for signal and background
    signals = [d for d in dataset_list if d.name in signalProcesses]
    backgrounds = [d for d in dataset_list if d.name in backgroundProcesses]

    # Case 1: there is only one variable                                                                                                                                  
    if variable_2 is None:
        for edge_1 in variableEdges_1:
            f = directions[0]
            count_signal = 0
            for signal in signals:
                signal_dataframe = signal.dataframe
                if cut_signal:
                    signal_dataframe = signal.dataframe.query(cut_signal)
                sel = (f*signal_dataframe[variable_1] > f*edge_1 )
                count_signal = count_signal + signal.scale*signal_dataframe[sel][weight_variable].sum()

            count_background = 0
            for background in backgrounds:
                background_dataframe = background.dataframe
                if cut_background:
                    background_dataframe = background.dataframe.query(cut_background)
                sel = (f*background_dataframe[variable_1] > f*edge_1 )
                count_background = count_background + background.scale*background_dataframe[sel][weight_variable].sum()

            fom = count_signal/np.sqrt(count_signal+count_background)

            foms.append(fom)
            cuts_1.append(edge_1)

        if len(foms)>0:
            res[0] = cuts_1[np.argmax(foms)]

    # Case 2: there are two variables                                                                                          
    else:
        for edge_1 in variableEdges_1:
            for edge_2 in variableEdges_2:
                print('testing {edge_1}, {edge_2}' )
                f_1 = directions[0]
                f_2 = directions[1]

                count_signal = 0
                for signal in signals:
                    signal_dataframe = signal.dataframe
                    if cut_signal:
                        signal_dataframe = signal.dataframe.query(cut_signal)
                    sel =  (f_1*signal_dataframe[variable_1] > f_1*edge_1 ) & (f_2*signal_dataframe[variable_2] > f_2*edge_2 )
                    count_signal = count_signal + signal.scale*signal_dataframe[sel][weight_variable].sum()

                count_background = 0
                for background in backgrounds:
                    background_dataframe = background.dataframe
                    if cut_background:
                        background_dataframe = background.dataframe.query(cut_background)
                    sel = (f_1*background_dataframe[variable_1] > f_1*edge_1 ) & (f_2*background_dataframe[variable_2] > f_2*edge_2 )
                    count_background = count_background + background.scale*background_dataframe[sel][weight_variable].sum()

                fom = count_signal/np.sqrt(count_signal+count_background)

                foms.append(fom)
                cuts_1.append(edge_1)
                cuts_2.append(edge_2)

        if len(foms)>0:
            res[0] = cuts_1[np.argmax(foms)]
            res[1] = cuts_2[np.argmax(foms)]

    return cuts_1, cuts_2, foms, res


def integral2D(arr, colID, rowID, directions = [1,1]):
    '''
    Row and col are matching what comes out of the hist2d
    '''
    integral = 0
    
    if directions == [1,1]:
        for col in arr[colID:]:
            integral = integral + col[rowID:].sum()
    
    if directions == [-1,1]:
        for col in arr[:colID]:
            integral = integral + col[rowID:].sum()
    
    if directions == [1,-1]:
        for col in arr[colID:]:
            integral = integral + col[:rowID].sum()
    
    if directions == [-1,-1]:
        for col in arr[:colID]:
            integral = integral + col[:rowID].sum()
    
    return integral



def calculateFoMFast(dataset_list,
                 signalProcesses,
                 backgroundProcesses,
                 variable_1,
                 variableEdges_1,
                 weight_variable,
                 variable_2=None,
                 variableEdges_2=None,
                 cut_background = None,
                 cut_signal = None,
                 directions = [1, 1] #1 for "variable > cut", -1 for "variable < cut"                                                                                                                                                                                                                                                                                                                                                                                                       
             ):
    cuts_1=[]
    cuts_2=[]
    foms=[]
    punzis = []
    dists = []
    effs = []
    rejs = [] 
    res = [None, None]
    res_punzi = [None, None]

    # prepare two separate lists of dataframes for signal and background                                                                                                                                               
    signals = [d for d in dataset_list if d.name in signalProcesses]
    backgrounds = [d for d in dataset_list if d.name in backgroundProcesses]

    counts_signal = 0
    edgx = 0
    edgy = 0
    for signal in signals:
        signal_dataframe = signal.dataframe
        if cut_signal:
            signal_dataframe = signal.dataframe.query(cut_signal)
        cnt, edgx, edgy, dummy = hist2d(signal_dataframe[variable_1], signal_dataframe[variable_2], weights=signal_dataframe[weight_variable], bins = (variableEdges_1, variableEdges_2) )
        counts_signal = np.array(counts_signal) + signal.scale*np.array(cnt)

    counts_background = 0
    for background in backgrounds:
        background_dataframe = background.dataframe
        if cut_background:
            background_dataframe = background.dataframe.query(cut_background)
        cnt, edgx, edgy, dummy = hist2d(background_dataframe[variable_1], background_dataframe[variable_2], weights=background_dataframe[weight_variable], bins = (variableEdges_1, variableEdges_2) )
        counts_background = np.array(counts_background) + background.scale*np.array(cnt)

    int_s = []
    int_b = []
    signal_counts = np.sum(counts_signal)
    background_counts = np.sum(counts_background)

    for xcutID in range(len(edgx)-1):
        for ycutID in range(len(edgy)-1):
            int_s = integral2D(counts_signal, xcutID, ycutID, directions)
            int_b = integral2D(counts_background, xcutID, ycutID, directions)
            eff = int_s/signal_counts
            rej = 1-int_b/background_counts
    
            fom = int_s/np.sqrt(int_s+int_b)
            punzi = eff/(3./2 + np.sqrt(int_b))
            dist = np.sqrt((1-eff)**2 + (1-rej)**2 )

            foms.append(fom)
            punzis.append(punzi)
            dists.append(dist)
            effs.append(eff)
            rejs.append(rej)

            cuts_1.append(edgx[xcutID])
            cuts_2.append(edgy[ycutID])
    
    if len(foms)>0:
        res[0] = cuts_1[np.argmax(foms)]
        res[1] = cuts_2[np.argmax(foms)]

    if len(punzis)>0:
        res_punzi[0] = cuts_1[np.argmax(punzis)]
        res_punzi[1] = cuts_2[np.argmax(punzis)]

    return cuts_1, cuts_2, foms, punzis, dists, effs, rejs, res, res_punzi


def plotFoM2D(xcuts, ycuts, fom, stepsize = 100):
    
    fig, axes = subplots(figsize=(8,8))
    ax = axes

    c = tricontourf(xcuts, ycuts, fom,  np.arange(min(fom), stepsize+max(fom), stepsize), cmap=cm.cividis)
    tricontour(xcuts, ycuts, fom,  levels=[0.99*max(fom)], linewidths=1, colors='k', linestyles = '--')

    posmax = fom.index(max(fom))


    plot(xcuts[posmax], ycuts[posmax], marker = 'x', markersize=10, color='k')
    print(xcuts[posmax], ycuts[posmax])

    xlim(min(xcuts), max(xcuts))
    ylim(min(ycuts), max(ycuts))
    
    colorbar(c)

    return ax
