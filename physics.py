import numpy as np
import scipy.integrate as itg


def massToX(sqrts, m):
    """Returns the fractional energy of the photon emitted in the process e+e- -> gamma X, calculated respect to the maximum photon energy
    
    Parameters
    ----------
    
    sqrts : float
    the energy in the CM frame of the e+e- pair

    m : float
    the mass of the state produced together with the photon.


    Returns
    -------

    x : float
    the fractional energy of the photon
    """

    return (1- m**2/sqrts**2)


def isrRadiator(sqrts, x):
    """Returns the NLO ISR radiator value for an emission of fractional energy x at CM energy of sqrts.
    Please note that the radiator is not a probability itself but rather a probability density. 
    An integration over x should be carried on in order to obtain a probability. However, the radiator
    is divergent (but integrable) at 0, so integration must be carried out carefully.
    Implemetation of the formula in Benayoun et al, Mod.Phys.Lett. A14 2605-2614, 1999
    
    Parameters
    ----------
    
    sqrts : float
    the energy in the CM frame of the e+e- pair [GeV]

    x : float
    the fractional energy of the ISR photon


    Returns
    -------

    W : float
    the NLO ISR radiator value 

    """



    me = 511e-6
    alpha = 1./137.
    L = 2*np.log(sqrts/me)
    z2 = 1.64493407
    z3 = 1.2020569
    b = 2*alpha*(L-1)/np.pi
    d2 = (9/8 -2*z2)*L**2 - (45/16 - 11*z2/2 -3*z3)*L - 6*z2*z2/5 - 6*z2*np.log(2) + 3*z2/8 + 57/12
    D = 1 + (alpha/np.pi)*(3*L/2 + (np.pi**2)/3 -2) + d2*(alpha/np.pi)**2
    W = D*b*x**(b-1)-0.5*b*(2-x)+(b*b/8)*( (2-x)*(3*np.log(1-x) -4*np.log(x)) - 4*(np.log(1-x)/x) -6 +x )

    return W


def isrXsection(sqrts, m, Gamma, Bee):
    """Returns the cross section for the ISR production of a narrow resonance V, in nb, integrated over the whole solid angle.

    Parameters
    ----------
    
    sqrts : float
    the energy in the CM frame of the e+e- pair [GeV]

    m : float
    the mass of V [GeV]

    Gamma : float
    the width of V [GeV]

    Bee : float
    the branching ratio for V -> e+e-


    Returns
    -------

    sigma : float
    the ISR cross section [nb] 
    """
    
    geVtoNb = 1./2.56819e-6
    
    return geVtoNb*isrRadiator(sqrts, massToX(sqrts, m))* (12*np.pi**2*Bee*Gamma/(m*sqrts**2 ))
    

